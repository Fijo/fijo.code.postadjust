﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fijo.Code.PostAdjust.TestAssembly
{
	public interface Interface {
		
	}

    public class Class
    {
	    public string Test { get; set; }
	    public string Data;

	    public Class(string data) {
		    Data = data;
	    }

		public int GetLength(string str) {
			return str.Length;
		}

		public class NestedClass {
			
		}
		public interface NestedInterface {
			string Info { get; set; }
			void Process(int count);
		}
    }

	public struct Struct {
		
	}

	public enum Enum {
		Hallo,
		Test
	}
}
