using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using Mono.Cecil;
using NUnit.Framework;

namespace Fijo.Code.PostAdjust {
	[TestFixture]
	public class CodeServiceTest {
		private AssemblyDefinition _assemblyDefinition;
		private Code _code;
		private ICodeService _codeService;

		[SetUp]
		public void SetUp() {
			_assemblyDefinition = AssemblyDefinition.ReadAssembly(@"R:\Fijo.Code.PostAdjust.TestAssembly\Fijo.Code.PostAdjust.TestAssembly.dll");
			_code = new Code {AssemblyDefinitions = _assemblyDefinition.IntoArray()};
			_codeService = new CodeService();
		}

		[TestCase("Class", "Interface", "NestedClass", "NestedInterface", "Struct", "Enum")]
		public void TestAssmbly_GetTypeDefs_MustReturnCorrectTypes(params string[] typeNames) {
			var actual = _codeService.GetTypeDefs(_code).Select(x => x.Name).ToArray();
			CollectionAssert.IsEmpty(actual.Except(typeNames));
		}

		[TestCase("Fijo.Code.PostAdjust.TestAssembly.dll")]
		public void TestAssmbly_GetModuleDefs_MustReturnCorrectModules(params string[] assemblyFileNames) {
			var actual = _codeService.GetModuleDefs(_code).Select(x => x.Name).ToArray();
			CollectionAssert.AreEqual(assemblyFileNames, actual);
		}
	}
}