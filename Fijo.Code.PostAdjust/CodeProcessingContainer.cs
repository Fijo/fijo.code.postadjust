namespace Fijo.Code.PostAdjust {
	public class CodeProcessingContainer {
		public Code Code { get; set; }
		public CodeProcessingOptions ProcessingOptions { get; set; }
	}
}