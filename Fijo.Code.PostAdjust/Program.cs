﻿using System;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;

namespace Fijo.Code.PostAdjust {
	class Program {
		static void Main(string[] args) {
			//var source = args[0];
			var assembly = AssemblyDefinition.ReadAssembly(@"R:\Fijo.Infrastructure.Documentation\Fijo.Infrastructure.Documentation.dll");
			var asmbly = Assembly.LoadFrom(@"R:\Fijo.Infrastructure.Documentation\Fijo.Infrastructure.Documentation.dll");
			foreach (var moduleDefinition in assembly.Modules) {
				foreach (var type in moduleDefinition.Types.Where(x =>x.Name != "<Module>")) {
					var currentTypeInstance = asmbly.GetType(type.FullName, true);
					Console.WriteLine(type.FullName);
					var customAttributes = currentTypeInstance.GetCustomAttributes(false);
					foreach (var attr in customAttributes.Where(x => x.GetType().FullName == typeof(RelatedLinkAttribute).FullName).Select(x => (dynamic)x)) {
						var i = 0;
						foreach (var link in attr.Links) {
							//new WebClient().DownloadFile(link, type.Name + "_" + i++);
							var tes = typeof (void);
							var attributeConstructor = moduleDefinition.Import(typeof (DescAttribute).GetConstructor(new[] {typeof (string)}));
							var customAttribute = new CustomAttribute(attributeConstructor);
							customAttribute.ConstructorArguments.Add(new CustomAttributeArgument(moduleDefinition.TypeSystem.String, string.Format("Ein link ... {0}", link)));
							type.CustomAttributes.Add(customAttribute);
						}
					}
				}
			}
			assembly.Write(@"R:\test.dll", new WriterParameters{WriteSymbols = true});

			Console.ReadLine();
			//AssemblyFactory.SaveAssembly(assembly, assemblyFilename); 
		}

		//private object GetValue(CustomAttribute x) {
		//	GetConstructor(x)
		//	x.ConstructorArguments.Select(x => x.Value)

		//	//.GetConstructor(args.Select(y => y.GetType()).ToArray())
		//}

		//private MethodDefinition GetConstructor(CustomAttribute x) {
		//	var typeReferences = x.ConstructorArguments.Select(z => z.Type).ToList();
		//	return x.AttributeType.Resolve().Methods
		//		.Where(y => y.Name == "ctor")
		//		.Single(y => y.Parameters.Select(z => z.ParameterType).SequenceEqual(typeReferences));
		//}
	}
}
