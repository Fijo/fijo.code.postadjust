using System;
using System.Collections.Generic;
using Mono.Cecil;

namespace Fijo.Code.PostAdjust {
	public interface ICodeService {
		IEnumerable<TypeDefinition> GetTypeDefs(Code code, TypeFilter filter = TypeFilter.Default);
		IEnumerable<MemberReference> GetObjRefs(Code code);
		IEnumerable<ModuleDefinition> GetModuleDefs(Code code);

		Type GetType(Code code, TypeDefinition typeDef);

	}
}