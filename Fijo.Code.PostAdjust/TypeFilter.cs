using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.PostAdjust {
	public enum TypeFilter {
		[Desc("Filters special Types like the �<Module>� type to be excluded")]
		Default,
		[Desc("Don�t filter anything")]
		All
	}
}