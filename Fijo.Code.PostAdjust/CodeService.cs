using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Mono.Cecil;

namespace Fijo.Code.PostAdjust {
	public class CodeService : ICodeService {
		#region Implementation of ICodeService
		public IEnumerable<TypeDefinition> GetTypeDefs(Code code, TypeFilter filter = TypeFilter.Default) {
			var typeDefinitions = GetModuleDefs(code).SelectMany(x => x.Types);
			if(filter == TypeFilter.Default) typeDefinitions = typeDefinitions.Where(x => !IgnoreTypeDef(x));
			return typeDefinitions;
		}

		private bool IgnoreTypeDef(TypeDefinition typeDef) {
			return typeDef.Name == "<Module>" && typeDef.Namespace == "";
		}

		public IEnumerable<MemberReference> GetObjRefs(Code code) {
			throw new NotImplementedException();
		}
		

		public IEnumerable<FieldReference> GetFields(Code code) {
			return GetTypeDefs(code).SelectMany(x => x.Fields);
		}


		public IEnumerable<ModuleDefinition> GetModuleDefs(Code code) {
			return code.AssemblyDefinitions.SelectMany(x => x.Modules);
		}

		public Type GetType(Code code, TypeDefinition typeDef) {
			var fullName = typeDef.FullName;
			var @namespace = typeDef.Namespace;
			return GetAssemblyForModule(code, typeDef.Module)
				.GetTypes()
				.Single(x => x.FullName == fullName && x.Namespace == @namespace);
		}

		private Assembly GetAssemblyForModule(Code code, ModuleDefinition moduleDef) {
			var assemlyFullName = moduleDef.Assembly.FullName;
			return code.Assemblies.Single(x => x.FullName == assemlyFullName);
		}
		#endregion
	}
}