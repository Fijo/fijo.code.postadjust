using System.Collections.Generic;
using System.Reflection;
using Mono.Cecil;

namespace Fijo.Code.PostAdjust {
	public class Code {
		public IList<AssemblyDefinition> AssemblyDefinitions { get; set; }
		public IList<Assembly> Assemblies { get; set; }
	}
}