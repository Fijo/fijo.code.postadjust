using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;

namespace Fijo.Code.PostAdjust {
	public interface ICodeProcessor : ICH<CodeProcessingContainer> {}
}